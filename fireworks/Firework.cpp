#include <random>

#include "Firework.hpp"

Firework::Firework(const int& screenW, const int& screenH)
    : mExploded(false), mOutOfFrame(false), mWidth(screenW), mHeight(screenH)
{
    static std::random_device rd;
    static std::mt19937 e2(rd());
    static std::uniform_real_distribution<> xrand(0.0, mWidth), yrand(mHeight, mHeight + 50),
                        xVel(-2, 2), yVel(6,10);
    mParticles.emplace_back( Feu(static_cast<float>(xrand(e2)), static_cast<float>(yrand(e2))) );
    mParticles.back().SetVel( static_cast<float>(xVel(e2)), static_cast<float>(yVel(e2)) );
    // static std::uniform_int_distribution<Uint8> colorRand(0x00, 0xFF);
    mColor = (0xFF          << 0*8) |
             (0x33          >> 1*8) |
             (0xFF          << 2*8) |
             (0xFF          << 3*8);
}

Firework::~Firework()
{
}

bool Firework::IsOut() const
{ return mOutOfFrame; }

void Firework::Draw(Framework& fw)
{
    for (auto& p : mParticles)
        p.Draw(fw, mColor);

}

void Firework::Update(const int& DT )
{
    if (!mExploded)
    {
        mParticles[0].Update( DT );
        float yvel;
        mParticles[0].GetVel(NULL, &yvel);
        if ( yvel <= 0 )
        {
            this->mExplode();
        }
    }
    else
    {
        mOutOfFrame = true;
        for (auto& p : mParticles)
        {
            p.Update( DT );
            mOutOfFrame = p.IsOut(mHeight) && mOutOfFrame;
        }
    }
}


void Firework::mExplode()
{
    mExploded = true;
    // Generate a static random number generator
    static std::random_device rd;
    static std::mt19937 e2(rd());
    static std::uniform_real_distribution<> angle(0.0, 2.0*3.14159265), rad(1,3);

    float last_x, last_y;
    mParticles[0].GetPos( &last_x, &last_y );
    // Del particle and spawn a bunch of them
    mParticles.pop_back();
    mParticles.reserve( N_PART );

    for (int i = 0; i < N_PART; ++i)
    {
        mParticles.emplace_back( Feu(last_x, last_y) );
        double rand_rad = rad(e2),
               rand_ang = angle(e2);
        mParticles[i].SetVel(   static_cast<float>( SDL_cos(rand_ang) * rand_rad),
                                static_cast<float>( SDL_sin(rand_ang) * rand_rad) + 2.0f);
    }
}
