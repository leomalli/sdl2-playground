#include "Feu.hpp"

Feu::Feu(const float& xPos_, const float& yPos_)
    : mPosX(xPos_), mPosY(yPos_), mVelX(.0f), mVelY(7.0f), mRad(3)
{
}

Feu::~Feu()
{
}


void Feu::Draw(Framework& fw, Uint32 color)
{
    int centerX = (int)mPosX, centerY = (int)mPosY;
    for (int j = centerY - mRad; j < centerY + mRad; ++j)
    {
        for (int i = centerX - mRad; i < centerX + mRad; ++i)
        {
            auto idx = j*fw.getWidth() + i;
            if (idx < 0 || idx > fw.pixels.size()) continue;

            fw.pixels[j*fw.getWidth() +i] = color;
        }
    }
}

bool Feu::IsOut(const int& sH) const
{
    return mPosY > 100 + sH;
}

// Takes dt in ms
void Feu::Update( const int& DT )
{
    mPosY -= mVelY * DT * 0.001f + GRAVITY * 0.5 * DT * 0.001f * DT * 0.001f;
    mPosX += mVelX * DT * 0.001f;
    mVelY -= GRAVITY * DT * 0.001f;
}

void Feu::GetVel(float* out_vx, float* out_vy) const
{
    if (out_vx) *out_vx = mVelX;
    if (out_vy) *out_vy = mVelY;
}

void Feu::GetPos( float* out_px, float* out_py ) const
{
    if (out_px) *out_px = mPosX;
    if (out_py) *out_py = mPosY;
}

void Feu::SetVel(const float& vx, const float& vy)
{ mVelX = vx; mVelY = vy; }
