#ifndef HPP_FEU
#define HPP_FEU
#include <SDL2/SDL.h>

#include "Framework.hpp"

#define GRAVITY 0.1f



class Feu
{
public:
    Feu(const float& xPos_, const float& yPos_ );
    ~Feu();

    void Update(const int& DT);
    void Draw(Framework&, Uint32 color);

    void GetVel(float* out_vx, float* out_vy) const;
    void SetVel(const float& vx, const float& vy);
    void GetPos(float* out_px, float* out_py) const;

    bool IsOut( const int& sH ) const;

private:
    float mPosX, mPosY;
    float mVelX, mVelY;
    int mRad;
};
#endif
