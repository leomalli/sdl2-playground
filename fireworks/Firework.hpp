#ifndef H_FIREWORK
#define H_FIREWORK
#include <SDL2/SDL.h>
#include <vector>

#include "Framework.hpp"
#include "Feu.hpp"

#define N_PART 100


class Firework
{
public:
    Firework(const int& sceenW, const int& screenH);
    ~Firework();

    void Draw(Framework&);
    void Update(const int& DT );
    bool IsOut() const;

private:
    bool mExploded, mOutOfFrame;
    int mWidth, mHeight;
    std::vector<Feu> mParticles;
public:
    Uint32 mColor{0};


    void mExplode();
};
#endif
