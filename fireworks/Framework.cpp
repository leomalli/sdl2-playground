#include "Framework.hpp"
#include <algorithm>


Framework::Framework( int width_, int height_ )
    :   mWidth( width_ ), mHeight( height_ ),
        pWindow( NULL ), pRenderer( NULL ), pTexture( NULL ),
        pixels( width_ * height_ )
{
    SDL_Init( SDL_INIT_VIDEO );
    SDL_CreateWindowAndRenderer(mWidth, mHeight, 0, &pWindow, &pRenderer);
    pTexture = SDL_CreateTexture(pRenderer,  SDL_PIXELFORMAT_ARGB8888,
                                            SDL_TEXTUREACCESS_STATIC,
                                            mWidth, mHeight);
    this->Clear();
    this->Render();
}


Framework::~Framework()
{
    SDL_DestroyTexture( pTexture );
    SDL_DestroyRenderer( pRenderer );
    SDL_DestroyWindow( pWindow );
    SDL_Quit();
}


void Framework::Update()
{
}

void Framework::Clear()
{

    std::fill_n(pixels.begin(), pixels.size(), 0);
    // std::fill(pixels.begin(), pixels.end(), 0);
    // memset(pixels.data(), 0, sizeof(Uint32)*pixels.size());
    SDL_SetRenderDrawColor(pRenderer, 0, 0, 0, 0);      // setting draw color
    SDL_RenderClear(pRenderer);      // Clear the newly created window
}

void Framework::Render()
{
    SDL_UpdateTexture(pTexture, NULL, pixels.data(), mWidth * sizeof(Uint32));
    SDL_RenderCopy( pRenderer, pTexture, NULL, NULL);
    SDL_RenderPresent(pRenderer);    // Reflects the changes done in the window.
}

SDL_Renderer* Framework::GetRenderer() const
{ return pRenderer; }
