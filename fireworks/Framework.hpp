#ifndef HPP_FRAMEWORK
#define HPP_FRAMEWORK
#include <SDL2/SDL.h>
#include <vector>

class Framework
{
public:
    Framework(int height_, int width_);
    ~Framework();

    void Update();
    void Render();
    void Clear();

    SDL_Renderer* GetRenderer() const;

    int getWidth() const {return mWidth;}


private:
    int mWidth, mHeight;
    SDL_Window* pWindow;
    SDL_Renderer* pRenderer;
    SDL_Texture* pTexture;
public:
    std::vector<Uint32> pixels;
};
#endif
