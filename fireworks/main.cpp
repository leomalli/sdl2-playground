#include <SDL2/SDL.h>
#include <vector>

#include "Framework.hpp"
#include "Firework.hpp"

struct Color
{
    Uint8 r = 0x00,
          g = 0x00,
          b = 0x00,
          a = 0xFF;
};

const int SCREEN_WIDTH  = 900;
const int SCREEN_HEIGHT = 600;
const int DT = 80;
const int N_FIRE = 10;
const Color BG_COLOR = { 0xFF, 0xFF, 0xFF, 0xFF };

int main( int argc, char* argv[] )
{

    Framework fw( SCREEN_WIDTH, SCREEN_HEIGHT );


    // Create a single firework
    std::vector<Firework> arr(N_FIRE, {SCREEN_WIDTH, SCREEN_HEIGHT} );




    // Initialize some states
    bool quit = false;
    SDL_Event e;

    while (!quit)
    {
        while ( SDL_PollEvent( &e ) != 0 )
        {
            if ( e.type == SDL_QUIT ) quit = true;
            else if ( e.type == SDL_KEYDOWN )
            {
                switch ( e.key.keysym.sym )
                {
                    case SDLK_q:
                        quit = true;
                        break;
                    default:
                        break;
                }
            }
        }

        for (auto& f : arr)
            f.Update( DT );

        // Check if any is out of bounds
        for (int i = 0; i < N_FIRE; ++i)
        {
            if (arr[i].IsOut())
            {
                arr[i] = Firework(SCREEN_WIDTH, SCREEN_HEIGHT);
            }
        }
        fw.Clear();
        for (auto& f : arr)
            f.Draw( fw );

        fw.Render();
    }

    return 0;
}
