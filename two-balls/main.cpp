#include <SDL2/SDL.h>
#include <vector>
#include <iostream>

#define WIN_WIDTH 600
#define WIN_HEIGHT 400
#define DT 10
#define PI 3.141592f

#define LOG(X) std::cout << X << '\n'

class Vec2i
{
public:
    Vec2i(): x(0), y(0) {}
    Vec2i(const int& x, const int& y): x(x), y(y) {}
    ~Vec2i() {}

    int normSquared() const
    { return x * x + y * y; }

    Vec2i add(const Vec2i& other) const
    { return Vec2i(this->x + other.x, this->y + other.y); }

    Vec2i substract(const Vec2i& other) const
    { return Vec2i(this->x - other.x, this->y - other.y); }

    float innerWith(const Vec2i& other) const
    { return x * (float)other.x + y * (float)other.y; }

    Vec2i mult(const int& a)
    { return Vec2i(x * a, y * a); }

    Vec2i mult(const float& a)
    {
        float tmp_x = (float)x * a, tmp_y = (float)y * a;
        return Vec2i((int)tmp_x, (int)tmp_y);
    }

    int x, y;
};

class Vec2f
{
public:
    Vec2f(): x(0), y(0) {}
    Vec2f(const float& x, const float& y): x(x), y(y) {}
    Vec2f(Vec2i other):x((float)other.x), y((float)other.y) {}
    ~Vec2f() {}

    float normSquared() const
    { return x * x + y * y; }

    Vec2f add(const Vec2f& other) const
    { return Vec2f(this->x + other.x, this->y + other.y); }

    Vec2f substract(const Vec2f& other) const
    { return Vec2f(this->x - other.x, this->y - other.y); }

    float innerWith(const Vec2f& other) const
    { return x * other.x + y * other.y; }

    Vec2f mult(const int& a)
    { return Vec2f(x * (float)a, y * (float)a); }

    Vec2f mult(const float& a)
    { return Vec2f(x * a, y * a); }

    Vec2f normalizeSquared()
    { return Vec2f(x / this->normSquared(), y / this->normSquared()); }

    float x, y;
};


class Framework{
public:
    // Contructor which initialize the parameters.
    Framework(int height_, int width_): height(height_), width(width_)
    {
        SDL_Init(SDL_INIT_VIDEO);       // Initializing SDL as Video
        SDL_CreateWindowAndRenderer(width, height, 0, &window, &renderer);
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);      // setting draw color
        SDL_RenderClear(renderer);      // Clear the newly created window
        SDL_RenderPresent(renderer);    // Reflects the changes done in the window.
    }

    // Destructor
    ~Framework()
    {
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
    }

    void clearRenderer()
    {
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);
        SDL_RenderPresent(renderer);
    }


    SDL_Renderer *renderer = NULL;      // Pointer for the renderer

private:
    int height;     // Height of the window
    int width;      // Width of the window
    SDL_Window *window = NULL;      // Pointer for the window
};


class Ball
{
public:

    Ball() {};

    Ball(int pos_x, int pos_y, int radius)
    : pos({pos_x, pos_y}), vel({0, 0}), radius(radius)
    {
        this->mass = radius * radius * PI;

    }
    ~Ball(){}

    void draw(SDL_Renderer* renderer)
    {
        SDL_SetRenderDrawColor( renderer, color.x, color.y, color.z, 255);

        for (int x = pos.x - radius; x < pos.x + radius; x++)
        {
            for (int y = pos.y - radius; y < pos.y + radius; y++)
            {
                int dist = (pos.y - y) * (pos.y - y) + (pos.x - x) * (pos.x - x);
                if ( dist <= radius * radius)
                    SDL_RenderDrawPoint( renderer, x, y);
            }
        }

    }

    void update()
    {
        pos.x += vel.x;
        if (pos.x - radius < 0)
        {
            pos.x -= 2*vel.x;
            vel.x *= -1;
        }
        else if (pos.x + radius > WIN_WIDTH)
        {
            pos.x -= 2*vel.x;
            vel.x *= -1;
        }

        pos.y += vel.y;
        if (pos.y - radius < 0)
        {
            pos.y -= 2*vel.y;
            vel.y *= -1;
        }
        else if (pos.y + radius > WIN_HEIGHT)
        {
            pos.y -= 2*vel.y;
            vel.y *= -1;
        }
    }

    void setVelocity(int x, int y)
    { vel.x = x; vel.y = y; }

    void setColor(int r, int g, int b)
    { color = { r, g, b}; }

    void collision(Ball& other)
    {
        Vec2i dist = other.pos.substract(this->pos);

        if (dist.normSquared() > (other.radius + radius) * (other.radius + radius)) return;
        // If they collided, we get back to last position, and update their velocities
        this->pBackInTime();
        other.pBackInTime();

        Vec2i   velDiff = vel.substract(other.vel),
                posDiff = pos.substract(other.pos);

        Vec2f normPosDiff = static_cast<Vec2f>(posDiff).normalizeSquared();

        Vec2f fac = normPosDiff.mult(velDiff.innerWith(posDiff));

        float   scale1 = (2*other.mass) / ( (this->mass + other.mass)),
                scale2 = (2*this->mass) / ( (this->mass + other.mass));


        Vec2f updated_this  = static_cast<Vec2f>(this->vel).substract(fac.mult(scale1));
        Vec2f updated_other = static_cast<Vec2f>(other.vel).add(fac.mult(scale2));

        this->vel = { (int)updated_this.x, (int)updated_this.y };
        other.vel = { (int)updated_other.x, (int)updated_other.y };
        this->update();
        other.update();
    }

    int mass;

private:
    struct Vec3i { int x, y, z; };

    Vec2i pos;
    Vec2i vel;
    int radius;
    Vec3i color = { 255, 0, 0 };

    void pBackInTime()
    {
        this->vel = (this->vel).mult(-1);
        this->update();
        this->vel = (this->vel).mult(-1);
    }

};


int main(int argc, char * argv[])
{

    // Creating the object by passing Height and Width value.
    Framework fw(WIN_HEIGHT, WIN_WIDTH);

    std::vector<Ball> balls(2);
    balls[0] = Ball(WIN_WIDTH / 2, WIN_HEIGHT / 2, 20);
    balls[1] = Ball(WIN_WIDTH / 4, 2 * WIN_HEIGHT / 3, 20);

    balls[0].setVelocity(3, 7);
    balls[1].setVelocity(-4, 2);
    balls[1].setColor(0, 255, 0);

    SDL_Event event;    // Event variable

    // Below while loop checks if the window has terminated using close in the
    //  corner.
    while(!(event.type == SDL_QUIT))
    {
        fw.clearRenderer();

        for (auto & ball : balls)
            ball.update();

        balls[0].collision(balls[1]);

        for (auto & ball : balls)
            ball.draw(fw.renderer);

        SDL_RenderPresent(fw.renderer);
        SDL_PollEvent(&event);  // Catching the poll event.
        SDL_Delay(DT);  // setting some Delay

    }
}
