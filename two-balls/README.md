# MY FIRST STEPS

This is my first programm written using SDL.
It is not meant as a great physic simulation of elastic shocks, neiter as an illustration of good programming practice.
The code is messy and the shocks tend to be energy diminishing (in general).

You can alter the ball sizes, which should change the behaviour of the shocks.
But it won't work very well, mostly due to the inherent inaccuracy resulted by chosing a big time step, and mostly doing integer computations.
