#ifndef HPP_CALCULATOR
#define HPP_CALCULATOR

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include <string>
#include <vector>
#include <functional>

class Calculator
{
public:
    Calculator(const int& winW, const int& winH);
    ~Calculator();

    void SetFont( TTF_Font* f );

    void Draw( SDL_Renderer* r );
    void Write( const std::string& str, const int& x, const int& y, const int& pxsize, const SDL_Color& C , SDL_Renderer* r );

    void Clicked( const int& x, const int&y );

private:
    int m_width, m_height;
    std::vector<double> m_memory;
    TTF_Font* m_font;
    std::string m_screen;
    bool m_isFloat, m_isPending;
    std::vector<std::string> m_KEYS;
    std::vector<bool> m_ISDIGIT;
    std::function<double(const double&, const double&)> m_currentOperation;

    void CellCenter( const int& i, const int& j, int* o_x, int* o_y ) const;
    void m_Press( const int& i, const int& j);
    void m_pushMemory();
    void m_Operation( const std::string& op );
    void m_reset();
};
#endif
