#include <cmath>


#include "Calculator.hpp"



Calculator::Calculator(const int& winW, const int& winH)
    :   m_width(winW), m_height(winH), m_memory({0.0}),
        m_font( NULL ), m_screen("0"), m_isFloat(false), m_isPending(false),
        m_KEYS( { "SQRT", "(-)", "C", "/", "1", "2", "3", "X", "4", "5", "6", "-", "7", "8", "9", "+", "0", "0", ".", "=" } ),
        m_ISDIGIT( {false, false, false, false, true, true, true, false, true, true, true, false, true, true, true, false, true, true, false, false})
{
    m_currentOperation = [](const double& a, const double& b) -> double { return 0.0; };
}


Calculator::~Calculator()
{}

void Calculator::SetFont( TTF_Font* f )
{
    m_font = f;
}

void Calculator::Draw( SDL_Renderer* r )
{
    Uint8 R = 0xFF, G = 0xFF, B = 0xFF, A = 0xFF;
    SDL_SetRenderDrawColor( r, R, G, B, A);

    // We need a 6x4 grid
    int cell_h = m_height / 6, cell_w = m_width / 4;

    // Draw all the lines
    for (int i = 1; i <= 5; ++i)
    {
        SDL_RenderDrawLine( r, 0, i * cell_h, m_width, i * cell_h);
    }

    // Draw the columns
    SDL_RenderDrawLine( r, cell_w, cell_h, cell_w, m_height - cell_h);
    SDL_RenderDrawLine( r, 2 * cell_w, cell_h, 2 * cell_w, m_height);
    SDL_RenderDrawLine( r, 3 * cell_w, cell_h, 3 * cell_w, m_height);

    int pxSize = 22;
    SDL_Color tCol = { 0xFF, 0x00, 0x00, 0xFF };
    int xx, yy;

    for (int i = 1; i <= 9; ++i)
    {
        this->CellCenter((i-1) % 3, 2 + (int)(i-1) / 3, &xx, &yy);
        this->Write( std::to_string(i) , xx, yy, pxSize, tCol, r );
    }

    this->CellCenter(0, 1, &xx, &yy);
    this->Write("SQRT" , xx - 30, yy, pxSize, tCol, r );
    this->CellCenter(1, 1, &xx, &yy);
    this->Write( "(-)" , xx - 20, yy, pxSize, tCol, r );
    this->CellCenter(2, 1, &xx, &yy);
    this->Write( "C" , xx, yy, pxSize, tCol, r );
    this->CellCenter(3, 1, &xx, &yy);
    this->Write( "/" , xx, yy, pxSize, tCol, r );
    this->CellCenter(3, 2, &xx, &yy);
    this->Write( "X" , xx, yy, pxSize, tCol, r );
    this->CellCenter(3, 3, &xx, &yy);
    this->Write( "-" , xx, yy, pxSize, tCol, r );
    this->CellCenter(3, 4, &xx, &yy);
    this->Write( "+" , xx, yy, pxSize, tCol, r );
    this->CellCenter(2, 5, &xx, &yy);
    this->Write( "." , xx, yy, pxSize, tCol, r );
    this->CellCenter(3, 5, &xx, &yy);
    this->Write( "=" , xx, yy, pxSize, tCol, r );

    this->Write( "0" , cell_w, yy, pxSize, tCol, r );


    // Draw screen
    this->Write( m_screen, 20, 20, pxSize, tCol, r);


    
    

    SDL_RenderPresent( r );

}

void Calculator::CellCenter( const int& i, const int& j, int* o_x, int* o_y ) const
{
    int cell_h = m_height / 6, cell_w = m_width / 4;
    *o_x = i * cell_w + cell_w / 2;
    *o_y = j * cell_h + cell_h / 2;
}

void Calculator::Write( const std::string& str, const int& x, const int& y, const int& pxsize , const SDL_Color& C, SDL_Renderer* r )
{
    SDL_Surface* textSurface = TTF_RenderText_Solid( m_font, str.c_str() , C );
    SDL_Texture* texture = SDL_CreateTextureFromSurface( r , textSurface );
    int tW = textSurface->w, tH = textSurface->h;
    SDL_FreeSurface( textSurface );

    SDL_Rect renderQuad = { x, y, tW, tH };
    SDL_RenderCopy( r , texture, NULL, &renderQuad );

    if ( texture != NULL )
    {
        SDL_DestroyTexture( texture );
        texture = NULL;
    }

}

void Calculator::m_Press( const int& i, const int& j)
{
    if (m_screen == "ERROR")
        this->m_reset();

    int idx = 4*(j-1) + i;
    if (m_ISDIGIT[idx])
    {
        if ( m_screen == "0" )
            m_screen = m_KEYS[ idx ];
        else if ( m_screen == "-0" )
            m_screen = "-" + m_KEYS[ idx ];
        else
            m_screen += m_KEYS[ idx ];
    }
    else if ( (m_KEYS[ idx ] == ".") && !m_isFloat )
    {
        m_screen += ".";
        m_isFloat = true;
    }
    else if ( m_KEYS[ idx ] == "(-)" )
    {
        if ( m_screen[0] == '-' )
            m_screen.erase( m_screen.begin() );
        else
            m_screen = "-" + m_screen;
    }
    else
    {
        this->m_pushMemory();
        this->m_Operation( m_KEYS[ idx ] );
    }

}

void Calculator::Clicked( const int& x, const int&y )
{
    int cell_h = m_height / 6, cell_w = m_width / 4;
    int i = (int)x / cell_w, j = (int)y / cell_h;
    if ( j == 0 ) return;

    this->m_Press(i, j);

}

void Calculator::m_pushMemory()
{ m_memory.push_back( std::stod( m_screen ) ); }


void Calculator::m_reset()
{
    m_isFloat = false;
    m_isPending = false;
    m_screen = "0";
    m_memory.erase(m_memory.begin() + 1, m_memory.end());
    m_memory[0] = 0.0;
}


void Calculator::m_Operation( const std::string& op )
{
    if ( op == "C" )
    {
        if (m_screen == "0")
            this->m_reset();
        else
        {
            m_isFloat = false;
            m_screen = "0";
        }
        return;
    }
    else if (m_isPending && op != "=")
    {
        m_screen = "ERROR";
        return;
    }


    if ( op == "SQRT" )
    {
        double result = std::sqrt( m_memory.back() );
        m_screen = std::to_string( result );
        m_isFloat = true;
        m_memory.back() = result;
        m_currentOperation = [](const double& a, const double& b) -> double { return std::sqrt(b); };
    }
    else if ( op == "/" )
    {
        m_screen = "0";
        m_isFloat = false;

        m_currentOperation = [](const double& a, const double& b) -> double { return a / b; };
        m_isPending = true;
    }
    else if ( op == "X" )
    {
        m_screen = "0";
        m_isFloat = false;

        m_currentOperation = [](const double& a, const double& b) -> double { return a * b; };
        m_isPending = true;
    }
    else if ( op == "-" )
    {
        m_screen = "0";
        m_isFloat = false;

        m_currentOperation = [](const double& a, const double& b) -> double { return a - b; };
        m_isPending = true;
    }
    else if ( op == "+" )
    {
        m_screen = "0";
        m_isFloat = false;

        m_currentOperation = [](const double& a, const double& b) -> double { return a + b; };
        m_isPending = true;
    }
    else if ( op == "=" )
    {
        auto end = m_memory.end();

        double res = m_currentOperation( *(end-2), *(end-1) );
        m_screen = std::to_string( res );
        m_isFloat = true;
        m_isPending = false;

    }
}
