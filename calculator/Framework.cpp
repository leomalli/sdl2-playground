#include "Framework.hpp"


Framework::Framework( int width_, int height_ )
    :   mWidth( width_ ), mHeight( height_ ),
        mBgCol( {0x00, 0x00, 0x00, 0xFF} ),
        pWindow( NULL ), pRenderer( NULL ), pFont( NULL )
{
    SDL_Init( SDL_INIT_VIDEO );
    if ( TTF_Init() == -1 )
    {
        fprintf( stderr, "Could not initialize fonts! TTF_Error: %s\n", TTF_GetError() );
        exit(1);
    }
    this->p_loadMedia();

    SDL_CreateWindowAndRenderer(mWidth, mHeight, 0, &pWindow, &pRenderer);
    this->Clear();
    this->Render();
}


Framework::~Framework()
{
    SDL_DestroyRenderer( pRenderer );
    pRenderer = NULL;

    SDL_DestroyWindow( pWindow );
    pWindow = NULL;

    TTF_CloseFont( pFont );
    pFont = NULL;

    TTF_Quit();
    SDL_Quit();
}


void Framework::p_loadMedia()
{
    pFont = TTF_OpenFont( "FreeMono.ttf", 30 );

}

TTF_Font* Framework::GetFont() const
{ return pFont; }

void Framework::Clear()
{
    SDL_SetRenderDrawColor(pRenderer, 0, 0, 0, 0);      // setting draw color
    SDL_RenderClear(pRenderer);      // Clear the newly created window
}


void Framework::Render()
{
    SDL_RenderPresent(pRenderer);    // Reflects the changes done in the window.
}


SDL_Renderer* Framework::GetRenderer() const
{ return pRenderer; }


void Framework::SetBGColor( const SDL_Color& C )
{ mBgCol = C; }
