#include <SDL2/SDL.h>

#include "Framework.hpp"
#include "Calculator.hpp"

const int SCREEN_WIDTH  = 600;
const int SCREEN_HEIGHT = 810;
const SDL_Color BG_COLOR = { 0x00, 0x00, 0x00, 0xFF };

int main( int argc, char* argv[] )
{

    Framework fw( SCREEN_WIDTH, SCREEN_HEIGHT );
    fw.SetBGColor( BG_COLOR );

    Calculator calc( SCREEN_WIDTH, SCREEN_HEIGHT );
    calc.SetFont( fw.GetFont() );

    // Initialize some states
    bool quit = false;
    SDL_Event e;

    while (!quit)
    {
        while ( SDL_PollEvent( &e ) != 0 )
        {
            if ( e.type == SDL_QUIT ) quit = true;
            else if ( e.type == SDL_KEYDOWN )
            {
                switch ( e.key.keysym.sym )
                {
                    case SDLK_q:
                        quit = true;
                        break;
                    default:
                        break;
                }
            }
            else if ( e.type == SDL_MOUSEBUTTONDOWN )
            {
                int x, y;
                SDL_GetMouseState(&x, &y);
                calc.Clicked(x, y);

            }
        }

        fw.Clear();
        calc.Draw( fw.GetRenderer() );
    }

    return 0;
}
