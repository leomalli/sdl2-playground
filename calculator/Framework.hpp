#ifndef HPP_FRAMEWORK
#define HPP_FRAMEWORK
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

class Framework
{
public:
    Framework(int height_, int width_);
    ~Framework();

    void Render();
    void Clear();

    SDL_Renderer* GetRenderer() const;
    TTF_Font* GetFont() const;

    void SetBGColor( const SDL_Color& C );


private:
    int mWidth, mHeight;
    SDL_Color mBgCol;
    SDL_Window* pWindow;
    SDL_Renderer* pRenderer;
    TTF_Font* pFont;

    void p_loadMedia();
};
#endif
